/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package vista_architetturale_gestoresmartfarm.entity2;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class EntityEmployerDAO {
	public static EntityEmployer loadEntityEmployerByORMID(String matricola) throws PersistentException {
		try {
			PersistentSession session = vista_architetturale_gestoresmartfarm.entity2.ProgettoPSSSAgosVersionPersistentManager.instance().getSession();
			return loadEntityEmployerByORMID(session, matricola);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static EntityEmployer getEntityEmployerByORMID(String matricola) throws PersistentException {
		try {
			PersistentSession session = vista_architetturale_gestoresmartfarm.entity2.ProgettoPSSSAgosVersionPersistentManager.instance().getSession();
			return getEntityEmployerByORMID(session, matricola);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static EntityEmployer loadEntityEmployerByORMID(String matricola, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = vista_architetturale_gestoresmartfarm.entity2.ProgettoPSSSAgosVersionPersistentManager.instance().getSession();
			return loadEntityEmployerByORMID(session, matricola, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static EntityEmployer getEntityEmployerByORMID(String matricola, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = vista_architetturale_gestoresmartfarm.entity2.ProgettoPSSSAgosVersionPersistentManager.instance().getSession();
			return getEntityEmployerByORMID(session, matricola, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static EntityEmployer loadEntityEmployerByORMID(PersistentSession session, String matricola) throws PersistentException {
		try {
			return (EntityEmployer) session.load(vista_architetturale_gestoresmartfarm.entity2.EntityEmployer.class, matricola);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static EntityEmployer getEntityEmployerByORMID(PersistentSession session, String matricola) throws PersistentException {
		try {
			return (EntityEmployer) session.get(vista_architetturale_gestoresmartfarm.entity2.EntityEmployer.class, matricola);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static EntityEmployer loadEntityEmployerByORMID(PersistentSession session, String matricola, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (EntityEmployer) session.load(vista_architetturale_gestoresmartfarm.entity2.EntityEmployer.class, matricola, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static EntityEmployer getEntityEmployerByORMID(PersistentSession session, String matricola, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (EntityEmployer) session.get(vista_architetturale_gestoresmartfarm.entity2.EntityEmployer.class, matricola, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryEntityEmployer(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = vista_architetturale_gestoresmartfarm.entity2.ProgettoPSSSAgosVersionPersistentManager.instance().getSession();
			return queryEntityEmployer(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryEntityEmployer(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = vista_architetturale_gestoresmartfarm.entity2.ProgettoPSSSAgosVersionPersistentManager.instance().getSession();
			return queryEntityEmployer(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static EntityEmployer[] listEntityEmployerByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = vista_architetturale_gestoresmartfarm.entity2.ProgettoPSSSAgosVersionPersistentManager.instance().getSession();
			return listEntityEmployerByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static EntityEmployer[] listEntityEmployerByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = vista_architetturale_gestoresmartfarm.entity2.ProgettoPSSSAgosVersionPersistentManager.instance().getSession();
			return listEntityEmployerByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryEntityEmployer(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From vista_architetturale_gestoresmartfarm.entity2.EntityEmployer as EntityEmployer");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryEntityEmployer(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From vista_architetturale_gestoresmartfarm.entity2.EntityEmployer as EntityEmployer");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("EntityEmployer", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static EntityEmployer[] listEntityEmployerByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryEntityEmployer(session, condition, orderBy);
			return (EntityEmployer[]) list.toArray(new EntityEmployer[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static EntityEmployer[] listEntityEmployerByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryEntityEmployer(session, condition, orderBy, lockMode);
			return (EntityEmployer[]) list.toArray(new EntityEmployer[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static EntityEmployer loadEntityEmployerByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = vista_architetturale_gestoresmartfarm.entity2.ProgettoPSSSAgosVersionPersistentManager.instance().getSession();
			return loadEntityEmployerByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static EntityEmployer loadEntityEmployerByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = vista_architetturale_gestoresmartfarm.entity2.ProgettoPSSSAgosVersionPersistentManager.instance().getSession();
			return loadEntityEmployerByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static EntityEmployer loadEntityEmployerByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		EntityEmployer[] entityEmployers = listEntityEmployerByQuery(session, condition, orderBy);
		if (entityEmployers != null && entityEmployers.length > 0)
			return entityEmployers[0];
		else
			return null;
	}
	
	public static EntityEmployer loadEntityEmployerByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		EntityEmployer[] entityEmployers = listEntityEmployerByQuery(session, condition, orderBy, lockMode);
		if (entityEmployers != null && entityEmployers.length > 0)
			return entityEmployers[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateEntityEmployerByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = vista_architetturale_gestoresmartfarm.entity2.ProgettoPSSSAgosVersionPersistentManager.instance().getSession();
			return iterateEntityEmployerByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateEntityEmployerByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = vista_architetturale_gestoresmartfarm.entity2.ProgettoPSSSAgosVersionPersistentManager.instance().getSession();
			return iterateEntityEmployerByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateEntityEmployerByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From vista_architetturale_gestoresmartfarm.entity2.EntityEmployer as EntityEmployer");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateEntityEmployerByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From vista_architetturale_gestoresmartfarm.entity2.EntityEmployer as EntityEmployer");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("EntityEmployer", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static EntityEmployer createEntityEmployer() {
		return new vista_architetturale_gestoresmartfarm.entity2.EntityEmployer();
	}
	
	public static boolean save(vista_architetturale_gestoresmartfarm.entity2.EntityEmployer entityEmployer) throws PersistentException {
		try {
			vista_architetturale_gestoresmartfarm.entity2.ProgettoPSSSAgosVersionPersistentManager.instance().saveObject(entityEmployer);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(vista_architetturale_gestoresmartfarm.entity2.EntityEmployer entityEmployer) throws PersistentException {
		try {
			vista_architetturale_gestoresmartfarm.entity2.ProgettoPSSSAgosVersionPersistentManager.instance().deleteObject(entityEmployer);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(vista_architetturale_gestoresmartfarm.entity2.EntityEmployer entityEmployer) throws PersistentException {
		try {
			vista_architetturale_gestoresmartfarm.entity2.ProgettoPSSSAgosVersionPersistentManager.instance().getSession().refresh(entityEmployer);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(vista_architetturale_gestoresmartfarm.entity2.EntityEmployer entityEmployer) throws PersistentException {
		try {
			vista_architetturale_gestoresmartfarm.entity2.ProgettoPSSSAgosVersionPersistentManager.instance().getSession().evict(entityEmployer);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
}
