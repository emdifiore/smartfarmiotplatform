/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package vista_architetturale_gestoresmartfarm.entity2;

public class EntityAmministratore {
	public EntityAmministratore() {
	}
	
	public boolean equals(Object aObj) {
		if (aObj == this)
			return true;
		if (!(aObj instanceof EntityAmministratore))
			return false;
		EntityAmministratore entityamministratore = (EntityAmministratore)aObj;
		if ((getCodiceFiscale() != null && !getCodiceFiscale().equals(entityamministratore.getCodiceFiscale())) || (getCodiceFiscale() == null && entityamministratore.getCodiceFiscale() != null))
			return false;
		return true;
	}
	
	public int hashCode() {
		int hashcode = 0;
		hashcode = hashcode + (getCodiceFiscale() == null ? 0 : getCodiceFiscale().hashCode());
		return hashcode;
	}
	
	private String codiceFiscale;
	
	private String nome;
	
	private String cognome;
	
	private String recapito;
	
	public void setCodiceFiscale(String value) {
		this.codiceFiscale = value;
	}
	
	public String getCodiceFiscale() {
		return codiceFiscale;
	}
	
	public String getORMID() {
		return getCodiceFiscale();
	}
	
	public void setNome(String value) {
		this.nome = value;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setCognome(String value) {
		this.cognome = value;
	}
	
	public String getCognome() {
		return cognome;
	}
	
	public void setRecapito(String value) {
		this.recapito = value;
	}
	
	public String getRecapito() {
		return recapito;
	}
	
	public EntityAmministratore(vista_architetturale_gestoresmartfarm.entity2.EntityAmministratoreDAO admin) {
		//TODO: Implement Method
		throw new UnsupportedOperationException();
	}
	
	public String toString() {
		return String.valueOf(getCodiceFiscale());
	}
	
}
