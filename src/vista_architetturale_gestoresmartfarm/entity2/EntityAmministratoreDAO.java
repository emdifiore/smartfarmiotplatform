/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package vista_architetturale_gestoresmartfarm.entity2;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class EntityAmministratoreDAO {
	public static EntityAmministratore loadEntityAmministratoreByORMID(String codiceFiscale) throws PersistentException {
		try {
			PersistentSession session = vista_architetturale_gestoresmartfarm.entity2.ProgettoPSSSAgosVersionPersistentManager.instance().getSession();
			return loadEntityAmministratoreByORMID(session, codiceFiscale);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static EntityAmministratore getEntityAmministratoreByORMID(String codiceFiscale) throws PersistentException {
		try {
			PersistentSession session = vista_architetturale_gestoresmartfarm.entity2.ProgettoPSSSAgosVersionPersistentManager.instance().getSession();
			return getEntityAmministratoreByORMID(session, codiceFiscale);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static EntityAmministratore loadEntityAmministratoreByORMID(String codiceFiscale, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = vista_architetturale_gestoresmartfarm.entity2.ProgettoPSSSAgosVersionPersistentManager.instance().getSession();
			return loadEntityAmministratoreByORMID(session, codiceFiscale, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static EntityAmministratore getEntityAmministratoreByORMID(String codiceFiscale, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = vista_architetturale_gestoresmartfarm.entity2.ProgettoPSSSAgosVersionPersistentManager.instance().getSession();
			return getEntityAmministratoreByORMID(session, codiceFiscale, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static EntityAmministratore loadEntityAmministratoreByORMID(PersistentSession session, String codiceFiscale) throws PersistentException {
		try {
			return (EntityAmministratore) session.load(vista_architetturale_gestoresmartfarm.entity2.EntityAmministratore.class, codiceFiscale);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static EntityAmministratore getEntityAmministratoreByORMID(PersistentSession session, String codiceFiscale) throws PersistentException {
		try {
			return (EntityAmministratore) session.get(vista_architetturale_gestoresmartfarm.entity2.EntityAmministratore.class, codiceFiscale);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static EntityAmministratore loadEntityAmministratoreByORMID(PersistentSession session, String codiceFiscale, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (EntityAmministratore) session.load(vista_architetturale_gestoresmartfarm.entity2.EntityAmministratore.class, codiceFiscale, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static EntityAmministratore getEntityAmministratoreByORMID(PersistentSession session, String codiceFiscale, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (EntityAmministratore) session.get(vista_architetturale_gestoresmartfarm.entity2.EntityAmministratore.class, codiceFiscale, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryEntityAmministratore(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = vista_architetturale_gestoresmartfarm.entity2.ProgettoPSSSAgosVersionPersistentManager.instance().getSession();
			return queryEntityAmministratore(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryEntityAmministratore(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = vista_architetturale_gestoresmartfarm.entity2.ProgettoPSSSAgosVersionPersistentManager.instance().getSession();
			return queryEntityAmministratore(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static EntityAmministratore[] listEntityAmministratoreByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = vista_architetturale_gestoresmartfarm.entity2.ProgettoPSSSAgosVersionPersistentManager.instance().getSession();
			return listEntityAmministratoreByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static EntityAmministratore[] listEntityAmministratoreByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = vista_architetturale_gestoresmartfarm.entity2.ProgettoPSSSAgosVersionPersistentManager.instance().getSession();
			return listEntityAmministratoreByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryEntityAmministratore(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From vista_architetturale_gestoresmartfarm.entity2.EntityAmministratore as EntityAmministratore");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryEntityAmministratore(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From vista_architetturale_gestoresmartfarm.entity2.EntityAmministratore as EntityAmministratore");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("EntityAmministratore", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static EntityAmministratore[] listEntityAmministratoreByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryEntityAmministratore(session, condition, orderBy);
			return (EntityAmministratore[]) list.toArray(new EntityAmministratore[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static EntityAmministratore[] listEntityAmministratoreByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryEntityAmministratore(session, condition, orderBy, lockMode);
			return (EntityAmministratore[]) list.toArray(new EntityAmministratore[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static EntityAmministratore loadEntityAmministratoreByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = vista_architetturale_gestoresmartfarm.entity2.ProgettoPSSSAgosVersionPersistentManager.instance().getSession();
			return loadEntityAmministratoreByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static EntityAmministratore loadEntityAmministratoreByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = vista_architetturale_gestoresmartfarm.entity2.ProgettoPSSSAgosVersionPersistentManager.instance().getSession();
			return loadEntityAmministratoreByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static EntityAmministratore loadEntityAmministratoreByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		EntityAmministratore[] entityAmministratores = listEntityAmministratoreByQuery(session, condition, orderBy);
		if (entityAmministratores != null && entityAmministratores.length > 0)
			return entityAmministratores[0];
		else
			return null;
	}
	
	public static EntityAmministratore loadEntityAmministratoreByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		EntityAmministratore[] entityAmministratores = listEntityAmministratoreByQuery(session, condition, orderBy, lockMode);
		if (entityAmministratores != null && entityAmministratores.length > 0)
			return entityAmministratores[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateEntityAmministratoreByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = vista_architetturale_gestoresmartfarm.entity2.ProgettoPSSSAgosVersionPersistentManager.instance().getSession();
			return iterateEntityAmministratoreByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateEntityAmministratoreByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = vista_architetturale_gestoresmartfarm.entity2.ProgettoPSSSAgosVersionPersistentManager.instance().getSession();
			return iterateEntityAmministratoreByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateEntityAmministratoreByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From vista_architetturale_gestoresmartfarm.entity2.EntityAmministratore as EntityAmministratore");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateEntityAmministratoreByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From vista_architetturale_gestoresmartfarm.entity2.EntityAmministratore as EntityAmministratore");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("EntityAmministratore", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static EntityAmministratore createEntityAmministratore() {
		return new vista_architetturale_gestoresmartfarm.entity2.EntityAmministratore();
	}
	
	public static boolean save(vista_architetturale_gestoresmartfarm.entity2.EntityAmministratore entityAmministratore) throws PersistentException {
		try {
			vista_architetturale_gestoresmartfarm.entity2.ProgettoPSSSAgosVersionPersistentManager.instance().saveObject(entityAmministratore);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(vista_architetturale_gestoresmartfarm.entity2.EntityAmministratore entityAmministratore) throws PersistentException {
		try {
			vista_architetturale_gestoresmartfarm.entity2.ProgettoPSSSAgosVersionPersistentManager.instance().deleteObject(entityAmministratore);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(vista_architetturale_gestoresmartfarm.entity2.EntityAmministratore entityAmministratore) throws PersistentException {
		try {
			vista_architetturale_gestoresmartfarm.entity2.ProgettoPSSSAgosVersionPersistentManager.instance().getSession().refresh(entityAmministratore);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(vista_architetturale_gestoresmartfarm.entity2.EntityAmministratore entityAmministratore) throws PersistentException {
		try {
			vista_architetturale_gestoresmartfarm.entity2.ProgettoPSSSAgosVersionPersistentManager.instance().getSession().evict(entityAmministratore);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
}
