/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package vista_architetturale_gestoresmartfarm.entity2;

public interface ORMConstants extends org.orm.util.ORMBaseConstants {
	final int KEY_ENTITYCOLTIVAZIONE_ENTITYAMMINISTRATORE = -488485751;
	
	final int KEY_ENTITYCOLTIVAZIONE_ENTITYSERRA = -108857657;
	
	final int KEY_ENTITYSERRA_ENTITYEMPLOYERS = 1347622530;
	
	final int KEY_ENTITYSERRA_ENTITYINTRUSIONES = -1015296513;
	
	final int KEY_ENTITYSERRA_ENTITYLETTURAS = -969438690;
	
	final int KEY_ENTITYSERRA_ENTITYSENSORES = -1827545282;
	
	final int KEY_ENTITYSMARTFARM_ENTITYCOLTIVAZIONES = 1128169686;
	
}
