package vista_architetturale_gestoresmartfarm.boundary;

import java.util.ArrayList;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import vista_architetturale_gestoresmartfarm.coordinator.CoordinatorIntrusione;

public class ProxySerra implements MqttCallback{
	
	private static final String brokerUrl ="tcp://broker.hivemq.com:1883";
	private static final String clientId = "clientId";
	private static final String topic = "SmartFarmLetture";
	
	private volatile static ProxySerra proxy = null;

	private ProxySerra() {
	}
	
	public static ProxySerra getInstance() {
		if (proxy == null) {
			synchronized(ProxySerra.class){ 
				if(proxy == null){
					proxy = new ProxySerra();
				}
			}
		}
		return proxy;
	}

	public int monitoraggioProssimita() {
        String topic        = "comando";
        String content      = "avvio";
        int qos             = 2;
        String broker       = "tcp://broker.hivemq.com:1883";
        String clientId     = "JavaSample";
        
        MemoryPersistence persistence = new MemoryPersistence();

        try {
            MqttClient sampleClient = new MqttClient(broker, clientId, persistence);
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setCleanSession(true);
            
            System.out.println("Connecting to broker: "+broker);
            sampleClient.connect(connOpts);
            System.out.println("Connected to broker");
            System.out.println("Publishing message:"+content);
            
            MqttMessage message = new MqttMessage(content.getBytes());
            message.setQos(qos);
            sampleClient.publish(topic, message);
            
            System.out.println("Message published");
            sampleClient.disconnect();
            sampleClient.close();
            //System.exit(0);
            
        } catch(MqttException me) {
        	
            System.out.println("reason "+me.getReasonCode());
            System.out.println("msg "+me.getMessage());
            System.out.println("loc "+me.getLocalizedMessage());
            System.out.println("cause "+me.getCause());
            System.out.println("excep "+me);
            
            me.printStackTrace();
            
            return -1;
        }
        
		return 1;
	}
	
	public int monitoraggioCam(){
		String topic        = "comando";
        String content      = "avvio cam";
        int qos             = 0;
        String broker       = "tcp://broker.hivemq.com:1883";
        String clientId     = "JavaSample";
        
        MemoryPersistence persistence = new MemoryPersistence();

        try {
            MqttClient sampleClient = new MqttClient(broker, clientId, persistence);
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setCleanSession(true);
            
            System.out.println("Connecting to broker: "+broker);
            sampleClient.connect(connOpts);
            System.out.println("Connected to broker");
            System.out.println("Publishing message:"+content);
            
            MqttMessage message = new MqttMessage(content.getBytes());
            message.setQos(qos);
            sampleClient.publish(topic, message);
            
            System.out.println("Message published");
            sampleClient.disconnect();
            sampleClient.close();
            //System.exit(0);
            
        } catch(MqttException me) {
        	
            System.out.println("reason "+me.getReasonCode());
            System.out.println("msg "+me.getMessage());
            System.out.println("loc "+me.getLocalizedMessage());
            System.out.println("cause "+me.getCause());
            System.out.println("excep "+me);
            
            me.printStackTrace();
            
            return -1;
        }
        
		return 1;
	}
	
	public int attivaAllarme() {
		String topic        = "comando";
        String content      = "attivo allarme";
        int qos             = 0;
        String broker       = "tcp://broker.hivemq.com:1883";
        String clientId     = "JavaSample";
        
        MemoryPersistence persistence = new MemoryPersistence();

        try {
            MqttClient sampleClient = new MqttClient(broker, clientId, persistence);
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setCleanSession(true);
            
            System.out.println("Connecting to broker: "+broker);
            sampleClient.connect(connOpts);
            System.out.println("Connected to broker");
            System.out.println("Publishing message:"+content);
            
            MqttMessage message = new MqttMessage(content.getBytes());
            message.setQos(qos);
            sampleClient.publish(topic, message);
            
            System.out.println("Message published");
            sampleClient.disconnect();
            sampleClient.close();
            //System.exit(0);
            
        } catch(MqttException me) {
        	
            System.out.println("reason "+me.getReasonCode());
            System.out.println("msg "+me.getMessage());
            System.out.println("loc "+me.getLocalizedMessage());
            System.out.println("cause "+me.getCause());
            System.out.println("excep "+me);
            
            me.printStackTrace();
            
            return -1;
        }
        
		return 1;
	}

	@Override
	public void connectionLost(Throwable arg0) {
	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken arg0) {
	}

	@Override
	public void messageArrived(String arg0, MqttMessage msg) throws Exception {
		// TODO Auto-generated method stub

		System.out.println("| Topic:" + topic);
		System.out.println("| Message: " + msg.toString());
		System.out.println("-------------------------------------------------");
		
		ProxySerraWorker worker = new ProxySerraWorker(msg);
		worker.start();
		
	}
	
	public static void main() {
		System.out.println("Subscriber running");
		new ProxySerra().subscribe(topic);
	}
	
	public void subscribe(String topic) {
		//logger file name and pattern to log
		MemoryPersistence persistence = new MemoryPersistence();

		try{
			
			MqttClient sampleClient = new MqttClient(brokerUrl, clientId, persistence);
			MqttConnectOptions connOpts = new MqttConnectOptions();
			connOpts.setCleanSession(true);

			System.out.println("checking");
			System.out.println("Mqtt Connecting to broker: " + brokerUrl);

			sampleClient.connect(connOpts);
			System.out.println("Mqtt Connected");

			sampleClient.setCallback(this);
			sampleClient.subscribe(topic);

			System.out.println("Subscribed");
			System.out.println("Listening");

		} catch (MqttException me) {
			System.out.println(me);
		}
		
	}
	
}
