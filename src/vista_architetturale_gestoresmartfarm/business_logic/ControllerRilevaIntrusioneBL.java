package vista_architetturale_gestoresmartfarm.business_logic;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.Semaphore;

import org.orm.PersistentException;

import vista_architetturale_gestoresmartfarm.entity2.*;

public class ControllerRilevaIntrusioneBL extends Thread{

	private int id;
	private float[] valoreProssimita;
	private ArrayList<Integer> img;
	private static final float criticalProxValue =5;

	private Semaphore proxsem;
	private Semaphore camsem;
	private Semaphore ripristinasem;
	private Semaphore waitsem ;



	protected int rilevaVolti() {
		//TODO with python script
		//throw new UnsupportedOperationException();
		return 0;
	}

	public synchronized int negaID() {
		//throw new UnsupportedOperationException();
		int code = 0;
		if (waitsem.getQueueLength() > 0)	{
			waitsem.release();
			code = 1;
		}
		else	code = -1;
		return code;

	}

	public synchronized int ripristinaRilevamento() {
		//throw new UnsupportedOperationException();
		int code = 0;

		//System.out.println(ripristinasem.getQueueLength());

		if (ripristinasem.getQueueLength() > 0)	{
			ripristinasem.release();
			code = 1;
		}
		else	code = -1;

		//System.out.println(ripristinasem.getQueueLength());

		return code;

	}
	public synchronized int confermaID() {
		//throw new UnsupportedOperationException();
		int code = 0;
		if (waitsem.getQueueLength() > 0)	{
			waitsem.release();
			code = 1;
		}
		else	code = -1;
		return code;

	}

	public int getID() {
		return id;
	}

	public void setID(int id) {
		this.id = id;
	}

	@Override
	public void run() {

		EntitySmartFarm esm = EntitySmartFarm.getInstance();

		EntityColtivazione colt = esm.getColtivazioneBySerraID(id);

		EntitySerra serra = null;
		
		try {
			serra = colt.getEntitySerraByORMID(id);
		} catch (PersistentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		EntitySensore proximity = serra.getSensoreProssimita();

		EntitySensore camera = serra.getSensoreFotografico();

		//ProxySerra proxyserra = ProxySerra.getInstance();

		while (true) {
			//MonitoraggioProssimità
			// invio messaggio alla serra
			try{
				proxsem.acquire();
				try {
					proximity.setValore(valoreProssimita);

					if(valoreProssimita[0] < criticalProxValue){
						// invio messaggio camera alla serra
						camsem.acquire();

						try{
							final float[] immagine = new float[img.size()];
							int index = 0;
							for (final Integer value: img)	immagine[index++] = value;
							camera.setValore(immagine);
							int ret = this.rilevaVolti();
							if (ret == 0 || ret == -1) {
								//New ProxyNotificationSystem
								String recapito = serra.getNotifyInformationResponsabile();
								if (ret == 0) {
									//ProxyNotificationSystem.inviosegnalazione();
									Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Europe/Rome"), Locale.ITALY);
									Date today = calendar.getTime();
									Date ora = calendar.getTime();
									int[] array = img.stream().mapToInt(i -> i).toArray();
									serra.addIntrusione(today, "selvaggina", array, ora);
								} else {
									//proxynotsys.nviosegnalazione();
									waitsem.acquire();
									try{
										if (true) // devo mettere if esito.equals(notRecognized)
										{
											Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Europe/Rome"), Locale.ITALY);
											Date today = calendar.getTime();
											Date ora = calendar.getTime();
											int[] array = img.stream().mapToInt(i -> i).toArray();
											serra.addIntrusione(today, "personale non autorizzato", array, ora);

											//invia messaggio allarme
											// get notidy admin 
											// manda notifica
										}
									} catch(Exception e){
										e.printStackTrace();
										waitsem.release();
										camsem.release();
										proxsem.release();
										waitsem.drainPermits();
										camsem.drainPermits();
										proxsem.drainPermits();
									}
								}

								System.out.println("Thread: "+this.id+" bloccato");
								ripristinasem.acquire();
								System.out.println("Thread: "+this.id+" Sbloccato");

							}
						}catch(Exception e){
							e.printStackTrace();
							System.out.println("here non dovrebbe comparire");
							camsem.release();
							proxsem.release();
							camsem.drainPermits();
							proxsem.drainPermits();
						}
					} 
				}catch(Exception e){
					e.printStackTrace();
					proxsem.release();
					proxsem.drainPermits();
				}
			}
			catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public ControllerRilevaIntrusioneBL(int id, ThreadGroup group, String threadName){
		super(group,threadName);
		this.id = id;
		valoreProssimita = new float[1];
		img = new ArrayList<Integer>();
		proxsem = new Semaphore(0);
		camsem = new Semaphore(0);
		ripristinasem = new Semaphore(0);
		waitsem = new Semaphore(0);
	}


	public void proximityValueArrived(float value) {
		valoreProssimita[0]=value;
		proxsem.release();
	}

	public void imgArrived(ArrayList<Integer> img) {
		this.img=img;
		camsem.release();
	}

}