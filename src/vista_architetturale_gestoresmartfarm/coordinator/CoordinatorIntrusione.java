package vista_architetturale_gestoresmartfarm.coordinator;

import java.util.ArrayList;

import vista_architetturale_gestoresmartfarm.business_logic.ControllerRilevaIntrusioneBL;
import vista_architetturale_gestoresmartfarm.entity2.EntityColtivazione;
import vista_architetturale_gestoresmartfarm.entity2.EntitySerra;
import vista_architetturale_gestoresmartfarm.entity2.EntitySmartFarm;

public class CoordinatorIntrusione {
	
	private volatile static CoordinatorIntrusione coordinator = null;
	private ThreadGroup groupBLThread;

	public int avviaRilevamento() {
		int code = 0;
		
		EntitySmartFarm esm = EntitySmartFarm.getInstance();
		EntityColtivazione[] colt = esm.getColtivazioniAttive();
		
		if(colt.length==0) {
			code=-1;
			return code;
		}

		for(int i=0; i<colt.length; i++) {
			
			EntitySerra serra = colt[i].getEntitySerra();
			int id = serra.getId();
			ControllerRilevaIntrusioneBL controller = new ControllerRilevaIntrusioneBL(id, groupBLThread, "worker"+i);
			controller.start();
			
			//System.out.println("ID :"+id);
		}
		
		return code;
	}

	public int negaID(int id, String matricola) {
		//throw new UnsupportedOperationException();
		int code = -1;
		
		EntitySmartFarm esm = EntitySmartFarm.getInstance();
		
		EntityColtivazione coltivazione = esm.getColtivazioneBySerraID(id);
		
		if(coltivazione == null) return code;
		
		String matResp = coltivazione.getEntitySerra().getResponsabile().getMatricola();
		
		if (!matricola.equals(matResp)) return code;
		
		ControllerRilevaIntrusioneBL controller = this.searchWorkerForID(id);
		
		if (controller == null) return code;
		
		code = controller.negaID();
		
		return code;
		
	}

	public int ripristinaRilevamento(int id, String matricola) {
		//throw new UnsupportedOperationException();
		int code = -1;
		
		EntitySmartFarm esm = EntitySmartFarm.getInstance();
		
		EntityColtivazione coltivazione = esm.getColtivazioneBySerraID(id);
		
		if(coltivazione == null) return code;
		
		String matResp = coltivazione.getEntitySerra().getResponsabile().getMatricola();
		
		if (!matricola.equals(matResp)) return code;
		
		ControllerRilevaIntrusioneBL controller = this.searchWorkerForID(id);
		
		if (controller == null) return code;
		
		code = controller.ripristinaRilevamento();
		
		return code;
	}

	public int confermaId(int id, String matricola) {
		//throw new UnsupportedOperationException();
		int code = -1;
		
		EntitySmartFarm esm = EntitySmartFarm.getInstance();
		
		EntityColtivazione coltivazione = esm.getColtivazioneBySerraID(id);
		
		if(coltivazione == null) return code;
		
		String matResp = coltivazione.getEntitySerra().getResponsabile().getMatricola();
		
		if (!matricola.equals(matResp)) return code;
		
		ControllerRilevaIntrusioneBL controller = this.searchWorkerForID(id);
		
		if (controller == null) return code;
		
		code = controller.confermaID();
		
		return code;
		
		// quindi se il code è negativo le matricole non combaciano, se positivo combaciano
	}

	protected ControllerRilevaIntrusioneBL searchWorkerForID(int idSerra) {
		
		ControllerRilevaIntrusioneBL[] th = new ControllerRilevaIntrusioneBL[groupBLThread.activeCount()];
		
		groupBLThread.enumerate(th);

		int i=0;
		while(i<th.length) {
			if(th[i].getID() == idSerra) return th[i];
			i++;
		}
		return null;
		
	}

	private CoordinatorIntrusione() {
		groupBLThread = new ThreadGroup("workerCoordinator");
	}

	public void notifyProximityBySerraID(int idSerra, float value) {
		
		ControllerRilevaIntrusioneBL thread = this.searchWorkerForID(idSerra);
		thread.proximityValueArrived(value);
		
	}

	public void notifyImageBySerraID(int idSerra, ArrayList<Integer> img) {
		
		ControllerRilevaIntrusioneBL thread = this.searchWorkerForID(idSerra);
		thread.imgArrived(img);
	}

	public static CoordinatorIntrusione getInstance() {
		if (coordinator == null) {
			synchronized(CoordinatorIntrusione.class){ //Serve la sincronizzazione visto che è solo lettura
				if(coordinator == null){
					coordinator = new CoordinatorIntrusione();
				}
			}
		}
		return coordinator;
	}
}