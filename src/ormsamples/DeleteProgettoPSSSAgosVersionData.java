/**
 * Licensee: 
 * License Type: Evaluation
 */
package ormsamples;

import org.orm.*;
public class DeleteProgettoPSSSAgosVersionData {
	public void deleteTestData() throws PersistentException {
		PersistentTransaction t = vista_architetturale_gestoresmartfarm.entity2.ProgettoPSSSAgosVersionPersistentManager.instance().getSession().beginTransaction();
		try {
			vista_architetturale_gestoresmartfarm.entity2.EntityIntrusione vista_Architetturale_GestoreSmartFarmEntity2EntityIntrusione = vista_architetturale_gestoresmartfarm.entity2.EntityIntrusioneDAO.loadEntityIntrusioneByQuery(null, null);
			// Delete the persistent object
			vista_architetturale_gestoresmartfarm.entity2.EntityIntrusioneDAO.delete(vista_Architetturale_GestoreSmartFarmEntity2EntityIntrusione);
			vista_architetturale_gestoresmartfarm.entity2.EntityLettura vista_Architetturale_GestoreSmartFarmEntity2EntityLettura = vista_architetturale_gestoresmartfarm.entity2.EntityLetturaDAO.loadEntityLetturaByQuery(null, null);
			// Delete the persistent object
			vista_architetturale_gestoresmartfarm.entity2.EntityLetturaDAO.delete(vista_Architetturale_GestoreSmartFarmEntity2EntityLettura);
			vista_architetturale_gestoresmartfarm.entity2.EntityEmployer vista_Architetturale_GestoreSmartFarmEntity2EntityEmployer = vista_architetturale_gestoresmartfarm.entity2.EntityEmployerDAO.loadEntityEmployerByQuery(null, null);
			// Delete the persistent object
			vista_architetturale_gestoresmartfarm.entity2.EntityEmployerDAO.delete(vista_Architetturale_GestoreSmartFarmEntity2EntityEmployer);
			vista_architetturale_gestoresmartfarm.entity2.EntitySerra vista_Architetturale_GestoreSmartFarmEntity2EntitySerra = vista_architetturale_gestoresmartfarm.entity2.EntitySerraDAO.loadEntitySerraByQuery(null, null);
			// Delete the persistent object
			vista_architetturale_gestoresmartfarm.entity2.EntitySerraDAO.delete(vista_Architetturale_GestoreSmartFarmEntity2EntitySerra);
			vista_architetturale_gestoresmartfarm.entity2.EntitySmartFarm vista_Architetturale_GestoreSmartFarmEntity2EntitySmartFarm = vista_architetturale_gestoresmartfarm.entity2.EntitySmartFarmDAO.loadEntitySmartFarmByQuery(null, null);
			// Delete the persistent object
			vista_architetturale_gestoresmartfarm.entity2.EntitySmartFarmDAO.delete(vista_Architetturale_GestoreSmartFarmEntity2EntitySmartFarm);
			vista_architetturale_gestoresmartfarm.entity2.EntityColtivazione vista_Architetturale_GestoreSmartFarmEntity2EntityColtivazione = vista_architetturale_gestoresmartfarm.entity2.EntityColtivazioneDAO.loadEntityColtivazioneByQuery(null, null);
			// Delete the persistent object
			vista_architetturale_gestoresmartfarm.entity2.EntityColtivazioneDAO.delete(vista_Architetturale_GestoreSmartFarmEntity2EntityColtivazione);
			vista_architetturale_gestoresmartfarm.entity2.EntityAmministratore vista_Architetturale_GestoreSmartFarmEntity2EntityAmministratore = vista_architetturale_gestoresmartfarm.entity2.EntityAmministratoreDAO.loadEntityAmministratoreByQuery(null, null);
			// Delete the persistent object
			vista_architetturale_gestoresmartfarm.entity2.EntityAmministratoreDAO.delete(vista_Architetturale_GestoreSmartFarmEntity2EntityAmministratore);
			vista_architetturale_gestoresmartfarm.entity2.EntitySensore vista_Architetturale_GestoreSmartFarmEntity2EntitySensore = vista_architetturale_gestoresmartfarm.entity2.EntitySensoreDAO.loadEntitySensoreByQuery(null, null);
			// Delete the persistent object
			vista_architetturale_gestoresmartfarm.entity2.EntitySensoreDAO.delete(vista_Architetturale_GestoreSmartFarmEntity2EntitySensore);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
		
	}
	
	public static void main(String[] args) {
		try {
			DeleteProgettoPSSSAgosVersionData deleteProgettoPSSSAgosVersionData = new DeleteProgettoPSSSAgosVersionData();
			try {
				deleteProgettoPSSSAgosVersionData.deleteTestData();
			}
			finally {
				vista_architetturale_gestoresmartfarm.entity2.ProgettoPSSSAgosVersionPersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
