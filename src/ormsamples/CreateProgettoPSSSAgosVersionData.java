/**
 * Licensee: 
 * License Type: Evaluation
 */
package ormsamples;

import org.orm.*;
public class CreateProgettoPSSSAgosVersionData {
	public void createTestData() throws PersistentException {
		PersistentTransaction t = vista_architetturale_gestoresmartfarm.entity2.ProgettoPSSSAgosVersionPersistentManager.instance().getSession().beginTransaction();
		try {
			vista_architetturale_gestoresmartfarm.entity2.EntityIntrusione vista_Architetturale_GestoreSmartFarmEntity2EntityIntrusione = vista_architetturale_gestoresmartfarm.entity2.EntityIntrusioneDAO.createEntityIntrusione();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : immagine
			vista_architetturale_gestoresmartfarm.entity2.EntityIntrusioneDAO.save(vista_Architetturale_GestoreSmartFarmEntity2EntityIntrusione);
			vista_architetturale_gestoresmartfarm.entity2.EntityLettura vista_Architetturale_GestoreSmartFarmEntity2EntityLettura = vista_architetturale_gestoresmartfarm.entity2.EntityLetturaDAO.createEntityLettura();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : umiditaSuolo, umiditaAria, temperatura
			vista_architetturale_gestoresmartfarm.entity2.EntityLetturaDAO.save(vista_Architetturale_GestoreSmartFarmEntity2EntityLettura);
			vista_architetturale_gestoresmartfarm.entity2.EntityEmployer vista_Architetturale_GestoreSmartFarmEntity2EntityEmployer = vista_architetturale_gestoresmartfarm.entity2.EntityEmployerDAO.createEntityEmployer();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : matricola
			vista_architetturale_gestoresmartfarm.entity2.EntityEmployerDAO.save(vista_Architetturale_GestoreSmartFarmEntity2EntityEmployer);
			vista_architetturale_gestoresmartfarm.entity2.EntitySerra vista_Architetturale_GestoreSmartFarmEntity2EntitySerra = vista_architetturale_gestoresmartfarm.entity2.EntitySerraDAO.createEntitySerra();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : entityLetturas, entityIntrusiones, entitySensores, entityEmployers
			vista_architetturale_gestoresmartfarm.entity2.EntitySerraDAO.save(vista_Architetturale_GestoreSmartFarmEntity2EntitySerra);
			vista_architetturale_gestoresmartfarm.entity2.EntitySmartFarm vista_Architetturale_GestoreSmartFarmEntity2EntitySmartFarm = vista_architetturale_gestoresmartfarm.entity2.EntitySmartFarmDAO.createEntitySmartFarm();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : entityColtivaziones
			vista_architetturale_gestoresmartfarm.entity2.EntitySmartFarmDAO.save(vista_Architetturale_GestoreSmartFarmEntity2EntitySmartFarm);
			vista_architetturale_gestoresmartfarm.entity2.EntityColtivazione vista_Architetturale_GestoreSmartFarmEntity2EntityColtivazione = vista_architetturale_gestoresmartfarm.entity2.EntityColtivazioneDAO.createEntityColtivazione();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : entitySerra, entityAmministratore
			vista_architetturale_gestoresmartfarm.entity2.EntityColtivazioneDAO.save(vista_Architetturale_GestoreSmartFarmEntity2EntityColtivazione);
			vista_architetturale_gestoresmartfarm.entity2.EntityAmministratore vista_Architetturale_GestoreSmartFarmEntity2EntityAmministratore = vista_architetturale_gestoresmartfarm.entity2.EntityAmministratoreDAO.createEntityAmministratore();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : codiceFiscale
			vista_architetturale_gestoresmartfarm.entity2.EntityAmministratoreDAO.save(vista_Architetturale_GestoreSmartFarmEntity2EntityAmministratore);
			vista_architetturale_gestoresmartfarm.entity2.EntitySensore vista_Architetturale_GestoreSmartFarmEntity2EntitySensore = vista_architetturale_gestoresmartfarm.entity2.EntitySensoreDAO.createEntitySensore();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : valore
			vista_architetturale_gestoresmartfarm.entity2.EntitySensoreDAO.save(vista_Architetturale_GestoreSmartFarmEntity2EntitySensore);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
		
	}
	
	public static void main(String[] args) {
		try {
			CreateProgettoPSSSAgosVersionData createProgettoPSSSAgosVersionData = new CreateProgettoPSSSAgosVersionData();
			try {
				createProgettoPSSSAgosVersionData.createTestData();
			}
			finally {
				vista_architetturale_gestoresmartfarm.entity2.ProgettoPSSSAgosVersionPersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
