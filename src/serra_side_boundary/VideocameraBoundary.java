package serra_side_boundary;

import java.util.ArrayList;

public class VideocameraBoundary implements ISensore {

	//valutare possibilita di mettere i metodi synchronized
	public VideocameraBoundary(){}
	
	@Override
	public Object getValue() {
		// TODO Auto-generated method stub
		ArrayList<Integer> img = new ArrayList<Integer>();
		
		//metodo python
		//per il momento faccio solo un ritorno di un vettore di 3 elementi di 0
		
		//img.add(0);
		//img.add(1);
		//img.add(0);
		//riesco a inviare foto anche con 200.000 mila elementi => risoluzioni alte
		for (int i = 0; i<200000;i++) img.add(2);
		return img;
	}

}
