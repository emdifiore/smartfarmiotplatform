package serra_side_boundary;

import java.util.ArrayList;

public class TestMainBoundary {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		ISensore sensProx = new ProximityBoundary();
		System.out.println(((Float)sensProx.getValue()).floatValue());
		
		ISensore cam = new VideocameraBoundary();
		System.out.println(((ArrayList<Integer>)cam.getValue()).get(0));
		
		IAttuatore alarm = new AllarmeBoundary();
		int code = alarm.doAction();
		System.out.println(code);
		
	}

}
