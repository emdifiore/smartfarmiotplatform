package serra_side_controller;

import serra_side_boundary.AllarmeBoundary;
import serra_side_boundary.IAttuatore;

public class CoordinatorAttuatoreSerra {

	private volatile static CoordinatorAttuatoreSerra c=null;

	private CoordinatorAttuatoreSerra (){

	}
	public static CoordinatorAttuatoreSerra getInstance(){
		if (c == null) {
			synchronized(CoordinatorAttuatoreSerra.class){
				if(c == null){
					c = new CoordinatorAttuatoreSerra();
				}
			}
		}
		return c;
	}
	
	public int attivaAllarme(){
		IAttuatore allarme = new AllarmeBoundary();
		return allarme.doAction();
	}
	
}
