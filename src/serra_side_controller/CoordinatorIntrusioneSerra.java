package serra_side_controller;

import java.util.ArrayList;

import serra_side_boundary.ISensore;
import serra_side_boundary.ProximityBoundary;
import serra_side_boundary.VideocameraBoundary;
import vista_architetturale_gestoresmartfarm.coordinator.CoordinatorIntrusione;

public class CoordinatorIntrusioneSerra {

	private volatile static CoordinatorIntrusioneSerra c=null;

	private CoordinatorIntrusioneSerra (){

	}

	public static CoordinatorIntrusioneSerra getInstance(){
		if (c == null) {
			synchronized(CoordinatorIntrusioneSerra.class){
				if(c == null){
					c = new CoordinatorIntrusioneSerra();
				}
			}
		}
		return c;
	}
	
	public Float richiediValoriProssimita(){
		ISensore proximity = new ProximityBoundary();
		return (Float) proximity.getValue();
		
	}
	
	public ArrayList<Integer> richiediValoriCam(){
		ISensore cam = new VideocameraBoundary();
		return (ArrayList<Integer>)cam.getValue();
	}

}

