package serra_side_proxy;

import java.util.ArrayList;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.json.*;

import serra_side_controller.CoordinatorAttuatoreSerra;
import serra_side_controller.CoordinatorIntrusioneSerra;

public class SerraWorker extends Thread {

	private String topicReply;

	private MqttMessage message ;

	public SerraWorker(MqttMessage message, String topicReply) {
		super();
		this.message = message;
		this.topicReply = topicReply+"Reply";
	}

	@Override
	public void run(){

		try {
			JSONObject json = (JSONObject) new JSONTokener(message.toString()).nextValue();
			String requestType = (String) json.get("tipo");

			switch(requestType){
			
			case "datiProssimita":
				try{
					CoordinatorIntrusioneSerra c1 = CoordinatorIntrusioneSerra.getInstance();
					float prox = c1.richiediValoriProssimita();
					
					JSONObject jsonReplyProx = new JSONObject();
					jsonReplyProx.put("tipo", "proxReply");
					jsonReplyProx.put("prossimita", prox);

					MemoryPersistence persistenceProx = new MemoryPersistence();
					MqttClient sampleClientProx = new MqttClient("tcp://broker.hivemq.com:1883", "serraWorker", persistenceProx);
					MqttConnectOptions connOptsProx = new MqttConnectOptions();
					connOptsProx.setCleanSession(true);
					sampleClientProx.connect(connOptsProx);
					MqttMessage messageProx = new MqttMessage(jsonReplyProx.toString().getBytes());
					messageProx.setQos(2); //setting qos
					sampleClientProx.publish(this.topicReply, messageProx);
					sampleClientProx.disconnect();

				}
				catch(Exception e){
					e.printStackTrace();
				}


				break;

			case "attivaAllarme":
				try{
					CoordinatorAttuatoreSerra c2 = CoordinatorAttuatoreSerra.getInstance();
					int code = c2.attivaAllarme();
					
					JSONObject jsonReplyCodeAl = new JSONObject();
					jsonReplyCodeAl .put("tipo", "returnCodeAlarm");
					jsonReplyCodeAl .put("value", code);

					MemoryPersistence persistenceAl = new MemoryPersistence();
					MqttClient sampleClientAlarm = new MqttClient("tcp://broker.hivemq.com:1883", "serraWorker", persistenceAl);
					MqttConnectOptions connOptsAl = new MqttConnectOptions();
					connOptsAl.setCleanSession(true);
					sampleClientAlarm.connect(connOptsAl);
					MqttMessage messageCode= new MqttMessage(jsonReplyCodeAl.toString().getBytes());
					messageCode.setQos(2);
					sampleClientAlarm.publish(this.topicReply, messageCode);
					sampleClientAlarm.disconnect();	
				}
				catch(Exception e){e.printStackTrace();}
				break;

			case "datiCam":
				try{
					CoordinatorIntrusioneSerra c3 = CoordinatorIntrusioneSerra.getInstance();
					ArrayList<Integer> img = c3.richiediValoriCam();
					
					JSONObject jsonReplyImg = new JSONObject();
					jsonReplyImg.put("tipo", "imgReply");
					JSONArray list = new JSONArray(img);
					jsonReplyImg.put("img", list);

					MemoryPersistence persistenceImg = new MemoryPersistence();
					MqttClient sampleClientImg = new MqttClient("tcp://broker.hivemq.com:1883", "serraWorker", persistenceImg);
					MqttConnectOptions connOptsImg = new MqttConnectOptions();
					connOptsImg.setCleanSession(true);
					sampleClientImg.connect(connOptsImg);
					MqttMessage messageImg = new MqttMessage(jsonReplyImg.toString().getBytes());
					messageImg.setQos(2); //setting qos
					sampleClientImg.publish(this.topicReply, messageImg);
					sampleClientImg.disconnect();
				}
				catch(Exception e){
					e.printStackTrace();}
				break;
			
			case "attivaIrrigazione":
				
				
				break;
				
			default:
				System.out.println("No match type");		
			}


		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
