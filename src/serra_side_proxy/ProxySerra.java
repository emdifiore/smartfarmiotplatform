package serra_side_proxy;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONObject;

public class ProxySerra implements MqttCallback{
	//private MqttClient client ;
	public ProxySerra(String topicName){
		skeleton(topicName);
	}
	
	public void skeleton(String topicName){
		try {
			MqttClient client = new MqttClient("tcp://broker.hivemq.com:1883", "SerraServer"+topicName);
			client.connect();
			client.setCallback(this);
			client.subscribe(topicName);
		} catch (MqttException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void connectionLost(Throwable arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void messageArrived(String arg0, MqttMessage arg1) throws Exception {
		// TODO Auto-generated method stub
		System.out.println("[SERRA SIDE] received message");
		SerraWorker worker = new SerraWorker(arg1, arg0);
		worker.start();
		
	}
	
	public int invioValori(JSONObject lettura){
		
		return 0;
	}
}
