package serra_side_proxy;

import java.util.ArrayList;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;



public class PahoDemo2 implements MqttCallback {

	MqttClient client;

	public PahoDemo2() {
	}

	public static void main(String[] args) {
		new PahoDemo2().doDemo();
		System.out.println("TEST");
		
		System.out.println("END TEST");
		
		ProxySerra serra = new ProxySerra("serra");
		
		try{
		/// fare invio cosi vediamo che sfaccimm riceviamo (invio richiesta prossimita)
		JSONObject jsonReplyProx = new JSONObject();
		//jsonReplyProx.put("tipo", "datiProssimita");
		//jsonReplyProx.put("tipo", "datiCam");
		jsonReplyProx.put("tipo", "attivaAllarme");
		MemoryPersistence persistenceProx = new MemoryPersistence();
		MqttClient sampleClientProx = new MqttClient("tcp://broker.hivemq.com:1883", "serraReq", persistenceProx);
		MqttConnectOptions connOptsProx = new MqttConnectOptions();
		connOptsProx.setCleanSession(true);
		sampleClientProx.connect(connOptsProx);
		MqttMessage messageProx = new MqttMessage(jsonReplyProx.toString().getBytes());
		messageProx.setQos(2); //setting qos
		sampleClientProx.publish("serra", messageProx);
		sampleClientProx.disconnect();
		
		
		
		}
		catch(Exception e )	{e.printStackTrace();}
		
	}

	public void doDemo() {
		try {
			client = new MqttClient("tcp://broker.hivemq.com:1883", "gg");
			client.connect();
			client.setCallback(this);
			client.subscribe("serraReply");
			
		} catch (MqttException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void connectionLost(Throwable cause) {
		// TODO Auto-generated method stub

	}

	@Override
	public void messageArrived(String topic, MqttMessage message) throws Exception {
		//System.out.println(message);
		//qua devo spacchettare il messaggio per vedere se la roba ricevuta è corretta
		//prima ricezione = prossimita
		JSONObject json = (JSONObject) new JSONTokener(message.toString()).nextValue();
		//System.out.println("[SMARTFARMSIMULATION RECEIVE PROX] "+json.get("prossimita"));
		
		//ora ricevo img
		//il codice sottostante funziona ma lo commento perche ora avvio l'allarme
		/*
		JSONArray list = (JSONArray) json.get("img");
		ArrayList<Integer> img = new ArrayList<Integer>();
		if (list != null) { 
			   for (int i=0;i<list.length();i++){ 
			    img.add(list.getInt(i));
			   } 
			}*/
		//System.out.println("[SMARTFARMSIMULATION RECEIVE IMG] "+img.get(0)+" "+ img.get(1)+" "+ img.get(2));
		
		// codice per ricezione codice intero di attivaAllarme
		System.out.println("[SMARTFARMSIMULATION RECEIVE code allarm] "+json.get("value"));
	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken token) {
		// TODO Auto-generated method stub

	}
}